#!/bin/bash
#SBATCH -p medium
#SBATCH -t 48:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --mem 100GB
#SBATCH -C cascadelake

source ~/.bashrc

python conv_study.py --ks 2 --kt 1 --ktls 1 --n_ref 8 --struct_mesh 2 --refinement_strategy space --ref_offset -9 --ref_offset2 1 --cgPsa 1 --geom kite --prob interpol
python conv_study.py --ks 2 --kt 1 --ktls 1 --n_ref 8 --struct_mesh 2 --refinement_strategy space --ref_offset -10 --ref_offset2 1 --cgPsa 1 --geom kite --prob interpol
