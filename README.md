# Reproduction material for "Geometrically Higher Order Unfitted Space-Time Methods for PDEs on Moving Domains: Geometry Error Analysis" by F. Heimann and C. Lehrenfeld

This repository contains the scripts necessary to reproduce the data of the numerical experiments of the paper.

The computations are based on the software ngsxfem, where we used the commit 2a2788631504740036e08ae13aba9a84cfe60294.

## Setup ngsxfem to run the examples.
Our necessary software can be supplied in several forms. Check out the [`github page`](http://github.com/ngsxfem/ngsxfem), where several installation routines, including compilation from source, are explained.

## Reproduction scripts
The commands to perform the numerical studies of the paper are collected in the bash files `runs.sh`, `runs_bndnes.sh` and `runs_cmd_sb.sh`. The main python script `conv_study.py` comes with a straightforward bash command line option parser. (C.f. also the sh-files for example calls.)
